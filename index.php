<?php

function __autoload($className){
    
    $file =  str_replace("\\", "/", $className) . ".php";
    include_once ($file);
}

use Account\Calculator\Add\Add;
use Account\Calculator\Division\Division;
use Account\Calculator\Subtract\Subtract;
use Account\Calculator\Multiple\Multiple;


$add = new Add();
echo $add ->addCalcuation(25, 5);
echo '<br>';

$subtract = new Subtract();
echo $subtract ->subtractCalculation(25, 5);
echo '<br>';

$multiple = new Multiple();
echo $multiple ->multipleCalcuation(25, 5);
echo '<br>';

$add = new Division();
echo $add ->divisionCalculation(25, 5);